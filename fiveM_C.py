import math


class FiveNumber(object):
    """


     class documentation
      Author:

      Class used to order a list of numbers read from a file

      Attributes

      samp: List
        a list that contain the values to be treat
      meanVal: real
        a real value that contain the mean value of all values
      stdVal : real
        a real value that contain the standard deviation value of all values
      medianVal: real
        a real value that contain the median value value of all numbers
      n_quartile_val: real

      nPercentVal: real


     Methods

      mean(self, sampl):
        method that read the numbers from the list samp and calculate the mean value

      std(self, sampl):
        method that read the numbers from the list samp and calculate the standard deviation value

      median(self, sampl):
        method that read the numbers from the list samp and calculate the median value

      n_quartile(self, sampl, quart):
       method that read the numbers from the list samp and calculate the x quartile given

      n_percentile(self, percentil):
        method that read the numbers from the list samp and calculate the x percentile given
    """

    def __init__(self, sample):
        """
        Constructor
        defining class attributes
        init all attributes
        """
        self.samp = sample  # Assign sample
        self.meanVal = 1.0  # init attributes
        self.stdVal = 1.0
        self.medianVal = 1.0
        self.n_quartile_val = 1.0
        self.nPercentVal = 1.0
        self.sizeArr = 1
        self.sum = 0.0
        self.sizeArr = len(self.samp)  # get list size
        self.sum2 = []
        self.sum3 = 0
        self.dataOrder = []
        self.middle = 0
        self.firstQ = 0
        self.thirdQ = 0
        self.quartile = 1
        self.percentileN = 1

    def mean(self, sampl):
        """
                method:  mean
                 parameters: self,

                function: it read the attribute samp that contain all numbers and calculate the mean. it print the mean
                return:
        """

        for x in range(self.sizeArr):
            self.sum = self.sum + self.samp[x]  # get total value

        self.meanVal = self.sum / self.sizeArr

        print("the mean is: " + str(self.meanVal))

    def std(self, sampl):
        """
                method:  std
                parameters: self,

                function: it read the attribute samp that contain all numbers and calculate the standard deviation. it print the value
                return:
        """

        self.mean(self.samp)  # compute mean

        for i in range(self.sizeArr):  # init list
            self.sum2.append(i)
            self.sum2[i] = self.samp[i] - self.meanVal  # get X-XP
            self.sum2[i] = abs(self.sum2[i])  # get abs value
            self.sum2[i] = self.sum2[i] * self.sum2[i]  # get x2
            self.sum3 = self.sum3 + self.sum2[i]  # get x2 total

        self.stdVal = self.sum3 / (self.sizeArr - 1)  # get x2 total/sampl-1 (Varianza)
        self.stdVal = math.sqrt(self.stdVal)

        print("the standard deviation is: " + str(self.stdVal))

    def median(self, sampl):
        """
                method:  median
                parameters: self,

                function: it read the attribute samp that contain all numbers and calculate the median value. it print the value
                return:
        """

        for j in range(self.sizeArr):
            self.dataOrder.append(j)  # init list

        self.dataOrder = sorted(self.samp)  # order data

        self.middle = self.sizeArr / 2

        if self.sizeArr % 2 == 0:  # get module

            self.medianVal = (self.dataOrder[int(self.middle)] + self.dataOrder[int(self.middle) + 1]) / 2
        else:

            self.medianVal = self.dataOrder[int(self.middle)]

        print("the median is: " + str(self.medianVal))

    def n_quartile(self, sampl, quart):

        """
                method:  n_quartile
                parameters: self, samp,quart

                function: it read the attribute samp that contain all numbers and calculate the x quartile given by
                user. it print the value find
                return:

        """

        for j in range(self.sizeArr):
            self.dataOrder.append(j)

        self.dataOrder = sorted(self.samp)  # order data

        if quart == 1:

            self.firstQ = self.sizeArr / 4

            if self.sizeArr % 4 == 0:

                self.n_quartile_val = (self.dataOrder[self.firstQ] + self.dataOrder[self.firstQ + 1]) / 2

            else:
                self.n_quartile_val = self.dataOrder[self.firstQ]

        if quart == 2:
            self.median(self.samp)
            self.n_quartile_val = self.medianVal

        if quart == 3:

            self.thirdQ = self.sizeArr - self.sizeArr / 4

            if self.sizeArr % 4 == 0:

                self.n_quartile_val = (self.dataOrder[int(self.thirdQ)] + self.dataOrder[int(self.thirdQ) + 1]) / 2

            else:

                self.n_quartile_val = self.dataOrder[int(self.thirdQ)]

        print("the N_Quartile is: " + str(self.n_quartile_val))

    def n_percentile(self, percentil):

        print("the precentil is: ")