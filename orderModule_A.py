class orderList(object):
    """
      class documentation
      Author:

      Class used to order a list of numbers read from a file

      Attributes
      ---------------
      path_to_read: str
        a string that contain the file location that shall be read
      path_to_write: str
        a string that contain the file location that shall be written with ordered list
      data : List
        a list that contain the numbers to be sorted and thar were read from file
      result: List
        a list that contain the list sorted and shall be written in file

     Methods
      -------
      set_input_data(self, path_to_read):
        method tat receive the file location were numbers are stored in .txt and separated by enter

      set_output_data(self, path_to_write):
        method that receive the file location were numbers ordered shall be stored in a file

      execute_merge_sort(self,data):
        method that sort the numbers the were read form file
    """

    def __init__(self):

        """
        Constructor
        defining class attributes

        init all attributes
        """
        self.path_to_read = ""  # File path
        self.path_to_write = ""  # file path
        self.data = []  # data to order
        self.result= [] # data to ordered

    def set_input_data(self, path_to_read):
        """
        method  set_input_data
        parameters: self, path_to_read

        function: it read the file located in "path_to_read", i.e "C:file.txt" and copy the values to a list
        name "data"
        return: it return the list that contain all values read from file
        """
        self.path_to_read = path_to_read
        try:
            file_in = open(self.path_to_read, 'r')

            with file_in as file_handle:
                file_contents = file_handle.readlines()

                for line in file_contents:
                  current_place = line[:-1]        # remove linebreak which is the last character of the string
                  try:
                   self.data.append(int(current_place))  # add item to the list
                  except ValueError:
                      print("This value is not correct: "+current_place)
                return self.data

        except:
            print("Exception occur File could not be opened")

    def set_output_data(self, path_to_write):

        """
                method  set_output_data
                parameters: self, path_to_write

                function: it write the file located in "path_to_write", i.e "C:fileOut.txt" and copy the values sorted to a list
                name "result"
                return: it return the file object
        """
        self.path_to_write = path_to_write

        file_out = open(self.path_to_write, 'w')
        with file_out as file_handle:
            file_handle.writelines("%s\n" % place for place in self.result)
        return file_handle



    def execute_merge_sort(self,data):
        """
                method  execute_merge_sort
                parameters: self, data

                function: it receive the list that contain all values to be sorted the apply the algorithm to sorted all
                values,
                return: it return a list
        """

        if len(data) <= 1:
            return data
        mid = int(len(data) / 2)
        left = self.execute_merge_sort(data[:mid])
        # print(left)
        right = self.execute_merge_sort(data[mid:])
        # print(right)
        return self.merge(left,right)


    def merge (self,left,right):

        self.result=[]
        i = 0
        j = 0

        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                self.result.append(left[i])
                i = i + 1
            else:
                self.result.append(right[j])
                j = j + 1
        self.result = self.result + left[i:]
        self.result = self.result + right[j:]

        return self.result