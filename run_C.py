"""
Created on Feb 15, 2020

@author:

file to test the function of FiveNumber class

    first: It create the object
         : Invoke all methods involved in class to test each method
         :you will see the result in console
"""
'module name'


from five.fiveM import FiveNumber

if __name__ == '__main__':
    pass

    sample = [68, 65, 62, 56, 56, 56, 53, 50, 50, 45, 44, 41, 40, 40, 39, 36, 30, 22, 30]

    r = FiveNumber(sample)  # create object

    r.std(sample)
    r.mean(sample)
    r.median(sample)
    r.n_quartile(sample, 3)
