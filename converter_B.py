"""
Created on Feb 19, 2020

@author: Daniel
"""


class Roman(object):
    """
    classdocs
    """

    def __init__(self):
        """
        Constructor
        """
        self.roma = {1000: 'M', 900: 'CM', 500: 'D', 400: 'CD', 100: 'C', 90: 'XC', 50: 'L', 40: 'XC', 10: 'X', 9: 'IX',
                     5: 'V', 4: 'IV', 1: 'I'}
        self.key = sorted((self.roma.keys()))
        self.number = 1
        self.out = ''

    def conversion(self):

        print("sorted((roma.keys():", self.key)
        self.key.reverse()

        self.number = int(input('write a number ?'))  # User input

        if self.number >= 4000:
            print('Error: too big number to convert')

        self.out = ''

        for key in self.key:
            while key <= self.number:
                self.out = self.out + self.roma[key]
                self.number = self.number - key
        print(self.out)
