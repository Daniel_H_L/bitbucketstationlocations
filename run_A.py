"""
  @package docstring

  class documentation
  Created on Mar 3, 2020

  @author: 

  Unit test:
  This file contain the test case for each method from orderList class

        it has tree test_case
        test_input_data:
            it check that set_input_data method return a list with all number to be sorted
        test_execute_merge:
            it check that sort algorithm works. First data.set_input_data must invoked and data.execute_merge_sort executed
        test_output_data:
            it check that test_output_data return a file with values
"""

import unittest

from orderPack.orderModule import orderList


list1 = [4, 5, 12, 20]
data = orderList()


class MyTestClass(unittest.TestCase):

    def test_input_data(self):
        self.assertIsInstance(data.set_input_data("read.txt"), list)

        return

    def test_execute_merge(self):
        data.set_input_data("read.txt")
        data.execute_merge_sort(data.data)
        self.assertEqual(data.result, list1)
        return

    def test_output_data(self):
        self.assertIsInstance(data.set_output_data("write.txt"), object)
        return


if __name__ == '__main__':
    pass

    unittest.main()

# print (data.set_input_data("read.txt"))
# print(data.execute_merge_sort(data.data))
# data.set_output_data("write.txt")
